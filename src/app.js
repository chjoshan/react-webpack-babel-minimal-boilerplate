import React from 'react';
import { render } from 'react-dom';

const title = 'My Minimal React Webpack Babel Setup';

render(<div>{title}</div>, document.getElementById('app'));
